package com.example.mathoperations

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.util.Log

class Operation () {
    var gcd_num : Int = 1
    fun gcd(firstNum : Int, secondNum : Int) : String {
        var temp : Int = 1
        var divisor : Int = 1
        val smallerBetweenTwo = if (firstNum > secondNum){
            secondNum
        } else {
            firstNum
        }

        while (temp <= smallerBetweenTwo){
            if (firstNum % temp == 0 && secondNum % temp == 0){
                divisor = temp;
            }
            temp++
        }
        gcd_num = divisor

        return ("The gcd of ${firstNum} and ${secondNum} is ${divisor}")
    }

    fun lcm(firstNum : Int, secondNum : Int) : String {
        return ("The lcm of  ${firstNum}  and  ${secondNum}  is  ${(firstNum*secondNum)/gcd_num}")
    }

    fun inOrNot(str : String) : String {
        if (str.indexOf('$') >= 0){
            return("Yes, given string contains $")
        }
        else{
            return("No, given string does not contain $")
        }
    }

    fun sumOfEvenNums(num : Int = 100) : Int {
        if (num <= 1) {
            return num
        }

        return num + sumOfEvenNums(num - 2)
    }

    fun reversion(intNum : Int) : String {
        var str = intNum.toString()

        str = str.reversed()
        var i : Int = 0

        val reversedNum = str.toInt()

        return ("Reversed number of given integer ${intNum} is ${reversedNum}")
    }

    fun palindrome(str : String) : String {
        val reversedStr = str.reversed()
        if (str.compareTo(reversedStr, true) == 0){
            return("Given string is palindrome")
        }
        else {
            return("Given string isn't palindrome")
        }
    }
}

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val Op = Operation()
        val text1 = findViewById<TextView>(R.id.gcd)
        text1.text = Op.gcd(24, 16)
        val text2 = findViewById<TextView>(R.id.lcm)
        text2.text = Op.lcm(24, 16)
        val text3 = findViewById<TextView>(R.id.isOrNot)
        text3.text = Op.inOrNot("Some string with $")
        val text4 = findViewById<TextView>(R.id.sum_of_evens)
        text4.text = "Sum of even nums from 1 to 100 equals ${Op.sumOfEvenNums().toString()}"
        val text5 = findViewById<TextView>(R.id.reversion)
        text5.text = Op.reversion(22003400)
        val text6 = findViewById<TextView>(R.id.palinrome)
        text6.text = Op.palindrome("level")
    }

}